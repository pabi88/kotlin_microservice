package eu.gpittech.kotlin_microservice_client

import eu.gpittech.kotlin_microservice_client.client.ClientController
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@EnableAutoConfiguration
@Configuration
internal class TrainerClientApplication {

	@Bean
	fun controller() = ClientController()
}


fun main(args: Array<String>) {
	runApplication<TrainerClientApplication>(*args)
}
