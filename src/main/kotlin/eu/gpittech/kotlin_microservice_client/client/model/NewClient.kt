package eu.gpittech.kotlin_microservice_client.client.model

import com.fasterxml.jackson.annotation.JsonCreator

/**
 * Representation of a User to create
 * @property username The username of the user
 * @property screenName The screen name of the user
 * @property email The email address of the user
 */
data class NewClient @JsonCreator constructor(
        val username: String,
        val name: String,
        val surname: String,
        val email: String
)