package eu.gpittech.kotlin_microservice_client.client.model

import java.time.Instant

data class Client(
        val username: String,
        val name: String,
        val surname: String,
        val email: String,
        val registered: Instant
)