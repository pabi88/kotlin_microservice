package eu.gpittech.kotlin_microservice_client.client

import eu.gpittech.kotlin_microservice_client.client.model.Client
import org.springframework.web.bind.annotation.*
import java.time.Instant

@RestController
class ClientController {
    @RequestMapping("/")
    fun helloSpringBoot() = "Hello SpringBoot"


    /** Get the details of a user */
    @RequestMapping("/user")
    fun getUser(): Client {
        val user = Client(
                username = "myUserName",
                name = "myName",
                surname = "mySurname",
                email = "mail@example.com",
                registered = Instant.now()
        )
        return user
    }
}